import UIKit

class ForcecastTableViewCell: UITableViewCell {

    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var descriptionWeather: UILabel!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(date : String, temperature : Double, description : String, address : String){
        temperatureLabel.text = "\(Int(temperature))°C"
        dateLabel.text = date
        descriptionWeather.text = description
        let url = URL(string: "https://openweathermap.org/img/wn/\(address)@2x.png")
        self.mainImageView.kf.setImage(with: url)
        
        
    }
}
