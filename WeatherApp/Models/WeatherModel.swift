import Foundation

struct WeatherModel {
    let weatherCondition: String
    let lon : Double
    let lat : Double
    let temperature: Double
    let feelsLike: Double
    let humidity: Int
    let pressure: Int
    let windSpeed: Double
    let windDeg: Int
    let dt: TimeInterval
    let iconUrl: String
    let cityName: String
    
    var temperatureString : String{
        return String(format: "%.1f", temperature) + "°C"
    }
    
    var feelsLikeString : String {
        return String(format: "%.1f", feelsLike) + "°C"
    }
    
    var weatherDirection: String  {
        switch windDeg {
        case 0...30, 341...360:
            return "N"
        case 31...75:
            return "NE"
        case 76...120:
            return "E"
        case 121...165:
            return "SE"
        case 166...210:
            return "S"
        case 211...255:
            return "SW"
        case 256...300:
            return "W"
        case 301...340:
            return "NW"
        default:
            return "N"
        }
    }
}
