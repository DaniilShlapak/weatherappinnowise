import Foundation

struct CitiesModel: Decodable {
    var citiesArray : [CitiesParametres]
}

struct CitiesParametres: Decodable {
    var name : String
}
