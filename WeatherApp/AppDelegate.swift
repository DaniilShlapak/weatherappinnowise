//
//  AppDelegate.swift
//  testWeatherAppInnowise
//
//  Created by Daniil Shlapak on 17.09.21.
//

import UIKit

 @main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        NetworkMonitorManager.shared.startMonitoring()
        // Override point for customization after application launch.
        return true
    }
}

