import Foundation

class Settings: Codable{
    
    var dict :  [String : String] = [:]
    
    init(dict: [String : String]){
        self.dict = dict
    }
    
    enum CodingKeys: String, CodingKey {
        case dict
    }

    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.dict = try container.decode([String : String].self, forKey: .dict)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(self.dict, forKey: .dict)
      }
}

class DarkModeSets: Codable {
    
    var darkMode : Bool
    
    init(darkMode : Bool){
        self.darkMode = darkMode
    }
    
    enum CodingKeys: String, CodingKey {
        case darkMode
    }

    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.darkMode = try container.decode(Bool.self, forKey: .darkMode)

    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(self.darkMode, forKey: .darkMode)

      }
}

class SavedCities: Codable {
    
    var citiesArray : [String]
    
    init(citiesArray : [String]){
        self.citiesArray = citiesArray
    }
    
    enum CodingKeys: String, CodingKey {
        case citiesArray
    }

    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.citiesArray = try container.decode([String].self, forKey: .citiesArray)

    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(self.citiesArray, forKey: .citiesArray)

      }
}
