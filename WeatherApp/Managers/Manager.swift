import Foundation
import UIKit

class Manager {
    
    static let shared = Manager()
    init (){}
    
    var savedCities = [String]()
    var city = "Minsk"
    var shareText : String = ""
    var savedData : [String : String]? = [:]
    
    func setWindDirection(_ direction : Int) -> String{
        switch direction {
        case 0...30, 341...360:
            return "N"
        case 31...75:
            return "NE"
        case 76...120:
            return "E"
        case 121...165:
            return "SE"
        case 166...210:
            return "S"
        case 211...255:
            return "SW"
        case 256...300:
            return "W"
        case 301...340:
            return "NW"
        default:
            return "N"
        }
    }
    
    func createAlert(title : String, with text : String, blur : UIVisualEffectView) -> UIAlertController{
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: { _ in
            blur.isHidden = true
        })
        let exitAction = UIAlertAction(title: "Exit", style: .cancel) { _ in
            exit(0)
        }
        alert.addAction(exitAction)
        alert.addAction(okAction)
        return alert
    }
    
    func convertDate(_ string : String ) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let date = dateFormatter.date(from: string) else {return ""}
        dateFormatter.dateFormat = "d MMM, HH:mm"
        let time = dateFormatter.string(from: date)
        return time
    }
    
}
