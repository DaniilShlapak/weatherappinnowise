import UIKit

class ForecastViewController: UIViewController {
    
    @IBOutlet weak var mainCityLabel: UILabel!
    @IBOutlet weak var blur: UIVisualEffectView!
    @IBOutlet weak var todayButton: UIView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var forecastTableView: UITableView!
    let dateFormatter = DateFormatter()
    
    var catchError : Bool? {
        didSet{
            if forecastViewModel.catchError.value == false {
                present(Manager.shared.createAlert(title: "Oops", with: "Something went wrong", blur: self.blur), animated: true) {
                    self.blur.isHidden = false
                }
            }
        }
    }
     
    var forecastArray : [ForecastList] = [] {
        didSet {
            if !forecastArray.isEmpty{
                self.forecastTableView.reloadData()
                self.indicator.stopAnimating()
                self.blur.isHidden = true
            }
        }
    }
    
    let forecastViewModel = ForecastViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configure()
    }
    
    private func configure(){
        self.forecastViewModel.getForecast()
        getForecast()
        navigationController?.setNavigationBarHidden(true, animated: true)
        self.addGestures()
        indicator.startAnimating()
        setCityLabel()
    }
    
    private func setCityLabel(){
        self.forecastViewModel.cityText.bind { city in
            self.mainCityLabel.text = String(city)
        }
    }
    
    private func getForecast(){
        self.forecastViewModel.catchError.bind { bool in
            self.catchError = bool
        }
        self.forecastViewModel.dataArray.bind { array in
            self.forecastArray = array
        }
    }
    
    private func addGestures(){
        let todayButtonRecoginzer = UITapGestureRecognizer(target: self, action: #selector(todayButtonPressed))
        self.todayButton.addGestureRecognizer(todayButtonRecoginzer)
    }
    
    @IBAction func todayButtonPressed(){
        self.navigationController?.popViewController(animated: true)
    }
}

extension ForecastViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecastArray.count - 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ForcecastTableViewCell", for: indexPath) as? ForcecastTableViewCell else {return UITableViewCell()}
        if !forecastArray.isEmpty{
            let array = self.forecastArray[indexPath.row + 1]
            let date = array.dt_txt
            let temp = array.main.temp
            let description = array.weather[0].main
            let address = array.weather[0].icon
            cell.configure(date: Manager.shared.convertDate(date), temperature: temp, description: description, address: address)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}
