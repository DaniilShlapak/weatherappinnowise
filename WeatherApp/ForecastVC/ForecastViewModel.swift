import Foundation
import UIKit

class ForecastViewModel: NSObject {

    var networkManager = NetworkManager()
    
    var catchError = Bindable<Bool>(true)
    var cityText = Bindable<String>("")
    var dataArray = Bindable<[ForecastList]>([])
    
    override init(){
        super.init()
        networkManager.forecastDelegate = self
    }
    
    func getForecast(){
        networkManager.fetchForecast(cityName: Manager.shared.city)
    }
}
extension ForecastViewModel: ForecastManagerDelegate {
    
    func didUpdateForecast(_ weatherManager: NetworkManager, forecast: ForecastModel) {
        self.dataArray.value = forecast.list
        self.cityText.value = forecast.city
    }
    
    func didFailWithError(error: Error) {
        self.catchError.value = false
        print(error)
    }
    
    
}
