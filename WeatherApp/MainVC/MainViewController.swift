import UIKit
import Kingfisher

class MainViewController: UIViewController {
    
    @IBOutlet weak var addCityImageView: UIImageView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var darkModeButton: UIImageView!
    @IBOutlet weak var weatherConditionImageView: UIImageView!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var weatherConditionLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var feelsLikeLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var windDirectionLabel: UILabel!
    @IBOutlet weak var forecastButton: UIView!
    @IBOutlet weak var blur: UIVisualEffectView!
    
    private var checkLoading : Bool = false {
        didSet{
            if self.checkLoading == true {
                self.indicator.stopAnimating()
                self.blur.isHidden = true
            }
        }
    }
    
    private var loadData : Bool? {
        didSet {
            if loadData == false {
                    self.cantLoadData()
            }
        }
    }
    
    private let mainViewModel = MainViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: true)
        mainViewModel.loadCities()
        checkInternt()
        addGestures()
        showWeather()
        mainViewModel.darkModeSaved()
    }
    
    private func checkInternt(){
        if NetworkMonitorManager.shared.isConnected {
            self.blur.isHidden = true
        }else{
            mainViewModel.noConnectionLabels()
            present(Manager.shared.createAlert(title: "No internet connection", with: "Can't update weather", blur: blur), animated: true) {
                self.blur.isHidden = false
            }
        }
    }
    
    private func cantLoadData(){
        present(Manager.shared.createAlert(title: "Oops!", with: "Something went wrong" , blur: self.blur ), animated: true, completion: {
            self.blur.isHidden = false
        })
    }
    
    private func addGestures(){
        let forecastGestureRecogizer = UITapGestureRecognizer(target: self, action: #selector(forecastButtonPressed))
        self.forecastButton.addGestureRecognizer(forecastGestureRecogizer)
        let darkModeRecognizer = UITapGestureRecognizer(target: self, action: #selector(setDarkMode))
        self.darkModeButton.addGestureRecognizer(darkModeRecognizer)
        
        let plusGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(plusButtonPressed))
        self.addCityImageView.addGestureRecognizer(plusGestureRecognizer)
    }
    
    @IBAction func forecastButtonPressed(){
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "ForecastViewController") as? ForecastViewController else {return}
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func setDarkMode(){
        mainViewModel.setDarkMode()
    }
    
    @IBAction func share(_ sender: UIButton) {
        self.present(mainViewModel.shareWeather(self.view), animated: true, completion: nil)
    }
    
    @IBAction func plusButtonPressed(){
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "CitiesViewController") as? CitiesViewController else {return}
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func showWeather(){
        self.indicator.startAnimating()
        self.blur.isHidden = false

        mainViewModel.darkModeIcon.bind { condition in
            self.darkModeButton.isHighlighted = condition
        }
        DispatchQueue.main.async {
            self.mainViewModel.weatherConditionText.bind { text in
                self.weatherConditionLabel.text = text
            }
            self.mainViewModel.temperatureText.bind { text in
                self.temperatureLabel.text = text
            }
            self.mainViewModel.cityName.bind { text in
                self.cityLabel.text = text
            }
            self.mainViewModel.humidityText.bind { text in
                self.humidityLabel.text = text
            }
            self.mainViewModel.fellsLikeText.bind { text in
                self.feelsLikeLabel.text = text
            }
            self.mainViewModel.pressureText.bind { text in
                self.pressureLabel.text = text
            }
            self.mainViewModel.windSpeedText.bind { text in
                self.windSpeedLabel.text = text
            }
            self.mainViewModel.windDirectionText.bind { text in
                self.windDirectionLabel.text = text
            }
            self.mainViewModel.iconText.bind { text in
                let url = URL(string: text)
                self.weatherIcon.kf.setImage(with: url)
            }
            self.mainViewModel.loadData.bind { condition in
                self.loadData = condition
            }
            self.mainViewModel.loadingWeather.bind { condition in
                self.checkLoading = condition
            }
        }
    }
}

extension MainViewController: CitiesViewControllerDelegate {
    
    func loadCity() {
        mainViewModel.getCityWeather()
        showWeather()
    }
}
