import Foundation
import UIKit
import CoreLocation
import Kingfisher

class MainViewModel : NSObject {
    
    var networkManager = NetworkManager()
    let locationManager = CLLocationManager()
    var loadingWeather = Bindable<Bool>(false)
    var loadData = Bindable<Bool>(true)
    var darkModeIcon = Bindable<Bool>(false)
    var weatherConditionText = Bindable<String>("")
    var temperatureText = Bindable<String>("")
    var cityName = Bindable<String>("")
    var humidityText = Bindable<String>("")
    var fellsLikeText = Bindable<String>("")
    var pressureText = Bindable<String>("")
    var windSpeedText = Bindable<String>("")
    var windDirectionText = Bindable<String>("")
    var iconText = Bindable<String>("")
    let savedData = UserDefaults.standard.value(Settings.self, forKey: "savedData")
    let savedDarkMode = UserDefaults.standard.value(DarkModeSets.self, forKey: "darkMode")
    let savedCities = UserDefaults.standard.value(SavedCities.self, forKey: "savedCities")
    
    override init() {
        super.init()
        configure()
    }
    
    func configure(){
        self.networkManager.weatherDelegate = self
        getUserLocation()
    }
    
    func getUserLocation(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func loadCities(){
        guard let cities = savedCities?.citiesArray else {return}
        Manager.shared.savedCities = cities
    }
    
    func darkModeSaved(){
        if let darkModeCondition = savedDarkMode?.darkMode{
            darkModeIcon.value = darkModeCondition
        }
        UIApplication.shared.windows.forEach { window in
            if darkModeIcon.value == true{
                window.overrideUserInterfaceStyle = .dark
            }else if self.darkModeIcon.value == false {
                window.overrideUserInterfaceStyle = .light
            }
        }
    }
    
    func noConnectionLabels(){
        Manager.shared.savedData = savedData?.dict
        guard let dict = Manager.shared.savedData else {return}
        self.cityName.value = dict["city"] ?? "N/A"
        self.temperatureText.value = dict["temperature"] ?? "N/A"
        self.weatherConditionText.value = dict["weatherCondition"] ?? "N/A"
        self.humidityText.value = dict["humidity"] ?? "N/A"
        self.fellsLikeText.value = dict["feelsLike"] ?? "N/A"
        self.pressureText.value = dict["pressure"] ?? "N/A"
        self.windSpeedText.value = dict["windSpeed"] ?? "N/A"
        self.windDirectionText.value = dict["windDirection"] ?? "N/A"
        self.iconText.value = dict["icon"] ?? "N/A"
    }
    
    @IBAction func setDarkMode(){
        UIApplication.shared.windows.forEach { window in
            if darkModeIcon.value == false{
                window.overrideUserInterfaceStyle = .dark
            }else if self.darkModeIcon.value == true {
                window.overrideUserInterfaceStyle = .light
            }
        }
        self.darkModeIcon.value = !darkModeIcon.value
        let savedValue = darkModeIcon.value
        let darkModeSets = DarkModeSets(darkMode: savedValue)
        UserDefaults.standard.set(encodable: darkModeSets, forKey: "darkMode")
    }
    
    func shareWeather(_ view : UIView)-> UIActivityViewController {
        let textToShare = [Manager.shared.shareText]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = view
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.mail, UIActivity.ActivityType.message]
        return activityViewController
    }
    
    func saveNoConnectionData(){
        Manager.shared.city = cityName.value.replacingOccurrences(of: " ", with: "%20")
        Manager.shared.shareText = "\(cityName.value): \(temperatureText.value)"
        Manager.shared.savedData?["city"] = cityName.value
        Manager.shared.savedData?["weatherCondition"] = weatherConditionText.value
        Manager.shared.savedData?["temperature"] = temperatureText.value
        Manager.shared.savedData?["humidity"] = humidityText.value
        Manager.shared.savedData?["feelsLike"] = fellsLikeText.value
        Manager.shared.savedData?["pressure"] = pressureText.value
        Manager.shared.savedData?["windSpeed"] = windSpeedText.value
        Manager.shared.savedData?["windDirection"] = windDirectionText.value
        Manager.shared.savedData?["icon"] = iconText.value
        guard let savedData = Manager.shared.savedData else {return}
        let sets = Settings(dict: savedData)
        UserDefaults.standard.set(encodable: sets, forKey: "savedData")
    }
    
    func getCityWeather(){
        networkManager.fetchWeather(cityName: Manager.shared.city)
    }
} 

extension MainViewModel: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            locationManager.stopUpdatingLocation()
            let lat = location.coordinate.latitude
            let long = location.coordinate.longitude
            networkManager.fetchWeather(latitude : lat, longtitude : long)
        }
    }
}
extension MainViewModel: WeatherManagerDelegate {
    
    func didUpdateWeather(_ weatherManager: NetworkManager, weather: WeatherModel) {
        self.cityName.value = weather.cityName
        self.temperatureText.value = weather.temperatureString
        self.fellsLikeText.value = weather.feelsLikeString
        self.humidityText.value = String(weather.humidity) + "%"
        self.pressureText.value = String(weather.pressure) + " hPa"
        self.weatherConditionText.value = weather.weatherCondition
        self.iconText.value = "https://openweathermap.org/img/wn/\(weather.iconUrl)@2x.png"
        self.windSpeedText.value = String(weather.windSpeed) + " m/s"
        self.windDirectionText.value = weather.weatherDirection
        self.loadData.value = true
        if self.cityName.value != ""{
            self.loadingWeather.value = true
        }
        self.saveNoConnectionData()
    }
    
    func didFailWithError(error: Error) {
        self.loadData.value = false
        self.noConnectionLabels()
    }
}
