import Foundation

struct WeatherData: Decodable  {
    var coord: Coord
    var weather: [WeatherLoc]
    var main: Main
    var wind: Wind
    var dt: TimeInterval
    var sys: Sys
    var name: String
}

struct Coord: Decodable {
    var lon: Double
    var lat: Double
}

struct WeatherLoc: Decodable {
    var main: String
    var icon : String
}

struct Main: Decodable {
    var temp: Double
    var feels_like : Double
    var humidity: Int
    var pressure: Int
}

struct Wind: Decodable {
    var speed: Double
    var deg : Int
}

struct Sys: Decodable {
    var country: String
}
