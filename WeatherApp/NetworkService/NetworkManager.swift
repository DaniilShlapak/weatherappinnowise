import Foundation
import CoreLocation

protocol WeatherManagerDelegate {
    func didUpdateWeather(_ weatherManager : NetworkManager, weather : WeatherModel)
    func didFailWithError(error : Error)
}

protocol ForecastManagerDelegate {
    func didUpdateForecast(_ weatherManager : NetworkManager, forecast : ForecastModel)
    func didFailWithError(error : Error)
}

protocol CitisManagerDelegate {
    func didUpdateCities(_ weatherManager : NetworkManager, cities : CitiesModel)
    func didFailWithError(error : Error)
}

struct NetworkManager {
    var weatherDelegate : WeatherManagerDelegate?
    var forecastDelegate : ForecastManagerDelegate?
    var citiesDelegate : CitisManagerDelegate?
    let weatherURL =  "https://api.openweathermap.org/data/2.5/weather?&units=metric&appid=037e78fd5a7cac6085401d301af54e36"
    
    let forecastURL = "https://api.openweathermap.org/data/2.5/forecast?&units=metric&appid=037e78fd5a7cac6085401d301af54e36"
    
    let citiesURL = "https://api.weatherapi.com/v1/search.json?key=1b49d2a850dc4d06842204329210811&q="
}


//MARK: - Get weather
extension NetworkManager {
    
    func fetchWeather(latitude : CLLocationDegrees, longtitude : CLLocationDegrees){
        
        let urlString = "\(weatherURL)&lat=\(latitude)&lon=\(longtitude)"
        
        performRequest(with : urlString)
        
    }
    
    func fetchWeather(cityName : String){
        
        let urlString = "\(weatherURL)&q=\(cityName)"
        
        performRequest(with : urlString)
        
    }
    
    func performRequest(with urlString : String){
        
        if let url = URL(string: urlString) {
            
            let session = URLSession.shared
            
            //3. Give session a task
            let task = session.dataTask(with: url) { (data, response, error) in
                if error != nil{
                    self.weatherDelegate?.didFailWithError(error: error!)
                    return
                }
                if let safedata = data {
                    if let weather = self.parseJson(safedata){
                        DispatchQueue.main.async {
                            self.weatherDelegate?.didUpdateWeather(self, weather : weather)
                        }
                    }
                }
            }
            
            task.resume()
        }
    }
    
    func parseJson(_ weatherData : Data) -> WeatherModel?{
        let decoder = JSONDecoder()
        
        do{
            let decodedData =  try decoder.decode(WeatherData.self, from: weatherData)
            let lon = decodedData.coord.lon
            let lat = decodedData.coord.lat
            let weatherCondition = decodedData.weather[0].main
            let temperature = decodedData.main.temp
            let feelsLike = decodedData.main.feels_like
            let humidity = decodedData.main.humidity
            let pressure = decodedData.main.pressure
            let windSpeed = decodedData.wind.speed
            let windDeg = decodedData.wind.deg
            let date = decodedData.dt
            let icon = decodedData.weather[0].icon
            let city = decodedData.name
            
            let weather = WeatherModel(weatherCondition: weatherCondition, lon: lon, lat: lat, temperature: temperature, feelsLike: feelsLike, humidity: humidity, pressure: pressure, windSpeed: windSpeed, windDeg: windDeg, dt: date, iconUrl: icon, cityName: city)
            return weather
            
        } catch {
            weatherDelegate?.didFailWithError(error: error)
            return nil
        }
    }
}

//MARK: - Get weather forecast
extension NetworkManager {
    
    func fetchForecast(cityName : String){
        
        let urlString = "\(forecastURL)&q=\(cityName)"
        
        performForecastRequest(with: urlString)
    }
    
    func performForecastRequest(with urlString : String){
        //1. create a URL
        if let url = URL(string: urlString) {
            //2. create URL session
            let session = URLSession.shared
            
            //3. Give session a task
            let task = session.dataTask(with: url) { (data, response, error) in
                if error != nil{
                    self.forecastDelegate?.didFailWithError(error: error!)
                    return
                }
                if let safedata = data {
                    if let weather = self.parseForecastJson(safedata){
                        DispatchQueue.main.async {
                            self.forecastDelegate?.didUpdateForecast(self, forecast: weather)
                        }
                    }
                }
            }
            task.resume()
        }
    }
    
    func parseForecastJson(_ weatherData : Data) -> ForecastModel?{
        let decoder = JSONDecoder()
        
        do{
            let decodedData =  try decoder.decode(DailyForecastData.self, from: weatherData)
            let array = decodedData.list
            let city = decodedData.city.name
            
            let forecast = ForecastModel(list: array, city: city)
            return forecast
            
        } catch {
            forecastDelegate?.didFailWithError(error: error)
            return nil
        }
    }
}

//MARK; - Get cities
extension NetworkManager {
    
    func fetchCities(cityName : String){
        
        let urlString = "\(forecastURL)\(cityName)"
        
        performCitiesRequest(with: urlString)
    }
    
    func performCitiesRequest(with urlString : String){
        //1. create a URL
        if let url = URL(string: urlString) {
            //2. create URL session
            let session = URLSession.shared
            
            //3. Give session a task
            let task = session.dataTask(with: url) { (data, response, error) in
                if error != nil{
                    self.citiesDelegate?.didFailWithError(error: error!)
                    return
                }
                if let safedata = data {
                    if let cities = self.parseCitiesJson(safedata){
                        DispatchQueue.main.async {
                            self.citiesDelegate?.didUpdateCities(self, cities: cities)
                        }
                    }
                }
            }
            task.resume()
        }
    }
    
    func parseCitiesJson(_ weatherData : Data) -> CitiesModel?{
        let decoder = JSONDecoder()
        
        do{
            let decodedData =  try decoder.decode(CitiesModel.self, from: weatherData)
            let array =  decodedData.citiesArray
            let cities = CitiesModel(citiesArray: array)
            return cities
            
        } catch {
            citiesDelegate?.didFailWithError(error: error)
            return nil
        }
    }
}
