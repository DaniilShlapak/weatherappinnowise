import Foundation

struct DailyForecastData : Decodable {
    var list : [ForecastList]
    var city : ForecastCity
}

struct ForecastCity : Decodable {
    var name : String
}

struct ForecastList : Decodable {
    var main : MainForecast
    var wind : WindForecast
    var weather : [WeatherForecast]
    var dt_txt : String
}

struct MainForecast : Decodable{
    var temp : Double
    var feels_like : Double
    var pressure : Int
    var humidity : Int
}

struct WindForecast : Decodable {
    var speed : Double
    var deg : Int
}

struct WeatherForecast : Decodable{
    var main : String
    var icon : String
}
