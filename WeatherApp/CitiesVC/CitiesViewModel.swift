import Foundation
import UIKit

class CitiesViewModel: NSObject {
    
    var citiesArray = Bindable<[CitiesParametres]>([])
    var networkManager = NetworkManager()
    
    
    override init(){
        super.init()
        self.networkManager.citiesDelegate = self
    }
//    func getCities(name: String, completion: @escaping ([CitiesModel])->()){
//        guard let urlForecast = URL(string: "https://api.weatherapi.com/v1/search.json?key=1b49d2a850dc4d06842204329210811&q=\(name)") else {return}
//        var requestForecast = URLRequest(url: urlForecast)
//        requestForecast.httpMethod = "GET"
//        let taskForecast = URLSession.shared.dataTask(with: requestForecast) { (data, response, error) in
//            if error == nil, let data = data {
//                do {
//                    let citiesData : [CitiesModel]?
//                    citiesData = try JSONDecoder().decode([CitiesModel].self, from: data)
//                    DispatchQueue.main.async {
//                        completion(citiesData ?? [])
//                    }
//
//                }catch {
//                    print(error)
//                }
//            }
//        }
//        taskForecast.resume()
//    }
    
    func getCities(name: String){
        self.networkManager.fetchCities(cityName: name)
    }
    
    func checkCity(city: String) -> Bool{
        var flag = true
        for i in Manager.shared.savedCities {
            if city.lowercased() == i.lowercased() {
                flag = false
                return flag
            }
        }
        return flag
    }
    
    func deleteEndOfString(text: String) -> String{
        if let index = text.range(of: ",")?.lowerBound {
            let substring = text[..<index]

            let string = String(substring)
            return string
        }
        return text
    }
    
    func createAlert() -> UIAlertController{
        let alert = UIAlertController(title: "City already added", message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        return alert
    }
}

extension CitiesViewModel: CitisManagerDelegate {
    func didUpdateCities(_ weatherManager: NetworkManager, cities: CitiesModel) {
        self.citiesArray.value = cities.citiesArray
    }
    
    func didFailWithError(error: Error) {
        print(error)
    }
    
    
}
