import UIKit

protocol CitiesViewControllerDelegate{
    func loadCity()
}

class CitiesViewController: UIViewController, UISearchControllerDelegate {
    
    @IBOutlet weak var citiesTableView: UITableView!
    
    var delegate : CitiesViewControllerDelegate?
    let citiesViewModel = CitiesViewModel()
    var arrayOfCity = [CitiesParametres]()
    
    lazy var searchController : UISearchController = {
        let sc = UISearchController(searchResultsController: nil)
        sc.searchResultsUpdater = self
        sc.obscuresBackgroundDuringPresentation = false
        sc.searchBar.placeholder = "Search cities..."
        sc.searchBar.sizeToFit()
        sc.searchBar.searchBarStyle = .prominent
        return sc
    }()
    
    private var searchBarIsEmpty: Bool {
        guard let text = searchController.searchBar.text else {return false}
        return text.isEmpty
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        definesPresentationContext = true
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.searchController = searchController
    }
    
}
extension CitiesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchBarIsEmpty {
            return Manager.shared.savedCities.count
        }else {
            return arrayOfCity.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CityTableViewCell", for: indexPath) as? CityTableViewCell else {return UITableViewCell()}
        if searchBarIsEmpty {
            cell.configure(text: Manager.shared.savedCities[indexPath.row])
        }else {
            let labelText = self.citiesViewModel.deleteEndOfString(text: arrayOfCity[indexPath.row].name)
            cell.configure(text: labelText)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if !searchBarIsEmpty {
            let labelText = self.citiesViewModel.deleteEndOfString(text: arrayOfCity[indexPath.row].name)
            if citiesViewModel.checkCity(city: labelText) {
                Manager.shared.savedCities.append(labelText)
                let cities = SavedCities(citiesArray: Manager.shared.savedCities)
                UserDefaults.standard.set(encodable: cities, forKey: "savedCities")
                searchController.searchBar.text = ""
            }else {
                present(citiesViewModel.createAlert(), animated: true, completion: nil)
            }
        }else {
            Manager.shared.city = Manager.shared.savedCities[indexPath.row]
            self.delegate?.loadCity()
            navigationController?.popViewController(animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if searchBarIsEmpty {
            if editingStyle == .delete {
                Manager.shared.savedCities.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .automatic)
                let cities = SavedCities(citiesArray: Manager.shared.savedCities)
                UserDefaults.standard.set(encodable: cities, forKey: "savedCities/Users/daniilshlapak/Downloads/учебник_по_грамматике_Митрошкиной.docx")
            }
        }
    }
}

extension CitiesViewController : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        self.citiesTableView.reloadData()
        guard let text = searchController.searchBar.text else {return}
        citiesViewModel.getCities(name: text.replacingOccurrences(of: " ", with: "%20")) {[weak self] cities in
            self?.arrayOfCity.removeAll()
            self?.arrayOfCity = cities
            self?.citiesTableView.reloadData()
        }
    }
}
